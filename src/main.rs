use std::io;
use std::io::prelude::*;
use std::fs::File;
use std::env;


fn main() {
    let mut buf = Vec::new();
    // skip first argument since it the app name
    env::args().skip(1)
        // map arguments to Box<Read>ers
        .map(|x| match x.as_str() {
            "-" => Box::new(io::stdin()) as Box<Read>,
            _ => Box::new(File::open(x).unwrap()) as Box<Read>,
        }
        )
        // chain all Box<Read>ers
        .fold(Box::new(io::empty()) as Box<Read>,
            |acc, stream| Box::new(acc.chain(stream)) as Box<Read>
        )
        // Read all data to buf
        .read_to_end(&mut buf).unwrap();

    if buf.len() == 0  {
        io::stdin().read_to_end(&mut buf).unwrap();
    }
    
    io::stdout().write_all(&mut buf).unwrap();


}
